# cplusplus

ISO standard compiled programming language. https://en.m.wikipedia.org/wiki/C++

[[_TOC_]]

# Tutorials
* [*C++ Tutorial*](https://www.w3schools.com/cpp/)
  w3schools

# Programming paradigm features
## Functional Programming
### Lists of materials and links
* [*Functional Programming in C++*
  ](https://github.com/graninas/cpp_functional_programming)
  (2020-06) Alexander Granin (graninas)

# Books
* [*C++20 - The Complete Guide*
  ](https://leanpub.com/cpp20)
  (2021-06) Nicolai M. Josuttis
* [*C++ Best Practices*
  ](https://leanpub.com/cppbestpractices)
  45ish Simple Rules with Specific Action Items for Better C++
  2021-01 Jason Turner
* [*Functional Programming in C++*
  ](https://worldcat.org/search?q=ti%3AFunctional+Programming+in+C%2B%2B)
  2019 Ivan Čukić
* *Hands-on functional programming with C++ : an effective guide to writing accelerated functional code using C++17 and C++20*
  2019 Alexandru Bolboaca (Packt Publishing)
* [*Introducing Functional Programming in C++*
  ](https://worldcat.org/search?q=ti%3AIntroducing+Functional+Programming+in+C%2B%2B)
  2018 Troy Miles
* *Learning C++ Functional Programming*
  2017 Wisnu Anggoro

# Features
## Memory management
### Resource Acquisition Is Initialization
* [*More C++ Idioms/Resource Acquisition Is Initialization*
  ](https://en.m.wikibooks.org/wiki/More_C%2B%2B_Idioms/Resource_Acquisition_Is_Initialization)

### Smart Pointers
* [C++ Smart Pointers](https://google.com/search?q=C%2B%2B+Smart+Pointers)

## Named Arguments
* [*Named Arguments in C++*
  ](https://www.fluentcpp.com/2018/12/14/named-arguments-cpp/)
  2018-12
* [*The Boost Parameter Library*
  ](https://www.boost.org/doc/libs/1_72_0/libs/parameter/doc/html/index.html)

# Libraries and uses
## Boost
* [Debian](https://tracker.debian.org/pkg/boost-defaults)
* [contract](http://boost.org/libs/contract)
* [iostreams](http://boost.org/libs/iostreams)

### By popularity
* [iostreams](http://boost.org/libs/iostreams)

## Curried Objects
* [*Curried Objects in C++*
  ](https://www.fluentcpp.com/2019/05/03/curried-objects-in-cpp/)

## Dependency Injection
* [c++ dependency injection
  ](https://google.com/search?q=c%2B%2B+dependency+injection)
* [*Write Your Own Dependency-Injection Container*
  ](https://www.fluentcpp.com/2019/06/07/write-your-own-dependency-injection-container/)
  2019-06 Nicolas Croad (Fluent C++)
* [*A Functional Alternative to Dependency Injection in C++*
  ](https://accu.org/journals/overload/25/140/pamudurthy_2403/)
  2017-08 Satprem Pamudurthy (Overload, 25(140):22-24)

## Named Arguments
* [*Named Arguments in C++*
  ](https://www.fluentcpp.com/2018/12/14/named-arguments-cpp/)

## Parser combinator
* [Spirit X3](https://www.boost.org/doc/libs/1_70_0/libs/spirit/doc/x3/html/index.html) @boost

## State machines
* [*Expressive Code for State Machines in C++*
  ](https://www.fluentcpp.com/2019/09/24/expressive-code-for-state-machines-in-cpp/)

# Debugging
## Statical analysis
### List of tools
* cppcheck [@Debian](https://packages.debian.org/fr/sid/cppcheck)
